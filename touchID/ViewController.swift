//
//  ViewController.swift
//  touchID
//
//  Created by Paweł Kowalczyk on 04.12.2016.
//  Copyright © 2016 Paweł Kowalczyk. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    let firstUseKey = "firstUse"

    override func viewDidLoad() {
        super.viewDidLoad()
        isFirstRun()
        
        //fingerprintAuth()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func isFirstRun(){
        
        let isNotFirstRun: Bool? = defaults.bool(forKey: firstUseKey)
        
        if isNotFirstRun == false {
            defaults.set(true, forKey: firstUseKey)
            allowBiometricLoginAlert()
        } else {
            let useBiometric: Bool = defaults.bool(forKey: "useBiometricForLogin")
            if useBiometric {
                fingerprintAuth()
            }
        }

    }
    
    func allowBiometricLoginAlert(){
        let alertViewController = UIAlertController(title: "Fingerprint", message: "Czy chcesz logować się za pomocą odcisku palca ?", preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Tak", style: .default, handler:
            {(action) in
                self.updateSettings(key: "useBiometricForLogin", value: true)
                self.fingerprintAuth()
        })
        
        let noAction = UIAlertAction(title: "Nie", style: .cancel, handler:
            {(action) in
                self.updateSettings(key: "useBiometricForLogin", value: false)
        })
        
        alertViewController.addAction(yesAction)
        alertViewController.addAction(noAction)
        DispatchQueue.main.async {
            self.present(alertViewController, animated: true, completion: nil)
  
        }
    }
    
    func updateSettings(key: String, value: Bool){
        defaults.set(value, forKey: key)
    }
    
    func fingerprintAuth(){
        let authenticationContext = LAContext()
        
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) else {
                showAlertViwIfNoBiometricSensorHasBeenDetected()
                return
        }
        
        authenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Zaloguj się odciskiem palca", reply: {(success, error) in
            if success {
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "showProtectedViewController", sender: self)
                }
            } else {
                if let error = error {
                    let message = error.localizedDescription
                    self.showAlertView(textMessage: message)
                }
            }
        })
    }
    
    func showAlertViwIfNoBiometricSensorHasBeenDetected(){
        showAlertView(textMessage: "Urządzenie nie posiada czytnika linii papilarnych")
    }
    
    func showAlertView(textMessage: String){
        let alertViewController = UIAlertController(title: "Błąd", message: textMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertViewController.addAction(okAction)
        
        DispatchQueue.main.async {
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


